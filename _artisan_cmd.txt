#------------ artisan
php artisan help make:resource
php artisan serve
php artisan db:seed

php artisan migrate
php artisan migrate:refresh
php artisan make:migration add_user_id_to_products

php artisan route:list

php artisan make:resource Product/ProductCollection
php artisan make:model Model/Project

artisan passport:install

*** auth
composer require laravel/ui
php artisan ui vue --auth
php artisan migrate
php artisan ui:auth
*** auth

php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan view:clear

*** run ui
npm run dev

php artisan make:request ProductRequest

#------------ package php
composer require laravel/passport
composer require laravel/ui